-- Jacek Łysiak
-- 31.03.2018
--
-- C-like language grammar 
-- in LBNF notation for BFN Converter


-- Comments format used in program files
comment         "//";
comment         "/*" "*/";

entrypoints     Program;

Entry.          Program ::= [ExtDecl] ;

(:[]).          [ExtDecl] ::= ExtDecl ;
(:).            [ExtDecl] ::= ExtDecl [ExtDecl] ;

-- Global scope
GVar.           ExtDecl ::= Decl ;
GFun.           ExtDecl ::= FunDef ;
GStructDec.     ExtDecl ::= StructSpec ;

DVar.           GenDecl ::= TypeSpec Ident ;
Declaration.    Decl ::= GenDecl ";" ;

[].             [Decl] ::= ;
(:).            [Decl] ::= Decl [Decl] ;

-- Types specifiers
-- void type for procedures
TVoid.          TypeSpec ::= "void";
TInt.           TypeSpec ::= "int";
TBool.          TypeSpec ::= "bool";
TStruct.        TypeSpec ::= "struct" Ident;
TArr.           TypeSpec ::= TypeSpec "[" Integer "]";               
TMap.           TypeSpec ::= "map" "<" TypeSpec "," TypeSpec ">";

StructDef.      StructSpec ::= "struct" Ident "{" [Decl] "}";

Func.           FunDef ::= "define" TypeSpec Ident "(" [GenDecl] ")" FunBody;

separator GenDecl ",";
separator FunDef  "";

FunctionBody.   FunBody ::= "{" [Decl] [FunDef] [Stmt] "return" ExprStmt "}";

StComp.         Stmt ::= CompStmt;
StExpr.         Stmt ::= ExprStmt;
StIf.           Stmt ::= IfStmt;
StIter.         Stmt ::= IterStmt;
StPrint.        Stmt ::= PrintStmt;

StCompSt.       CompStmt ::= "{" [Decl] [Stmt] "}";

StExprEnd.      ExprStmt ::= ";";
StExprSt.       ExprStmt ::= Expr ";";

StIfOnly.       IfStmt ::= "if" "(" Expr ")" CompStmt;
StIfElse.       IfStmt ::= "if" "(" Expr ")" CompStmt "else" CompStmt;

StIterWhile.    IterStmt ::= "while" "(" Expr ")" Stmt;
StIterForTwo.   IterStmt ::= "for" "(" ExprStmt ExprStmt ")" Stmt;
StIterForThree. IterStmt ::= "for" "(" ExprStmt ExprStmt Expr ")" Stmt;

PrintBase.      PrintStmt ::= "print" "(" Expr ")" ";";

[].             [Stmt] ::= ;
(:).            [Stmt] ::= Stmt [Stmt];


-- Comma rule is useless
ExComma.        Expr ::= Expr "," Expr1;

ExAssign.       Expr1 ::= Expr4 AssignOp Expr1;
ExEq.           Expr2 ::= Expr2 "==" Expr3;
ExNEq.          Expr2 ::= Expr2 "!=" Expr3;
ExLTh.          Expr3 ::= Expr3 "<" Expr4;
ExGTh.          Expr3 ::= Expr3 ">" Expr4;
ExLETh.         Expr3 ::= Expr3 "<=" Expr4;
ExGETh.         Expr3 ::= Expr3 ">=" Expr4;
ExAdd.          Expr4 ::= Expr4 "+" Expr5;
ExSub.          Expr4 ::= Expr4 "-" Expr5;
ExMul.          Expr5 ::= Expr5 "*" Expr6;
ExDiv.          Expr5 ::= Expr5 "/" Expr6;
ExNeg.          Expr6 ::= "-" Expr6;
ExMemb.         Expr7 ::= Expr7 "." Ident;
ExArr.          Expr7 ::= Expr7 "[" Expr "]";
ExFun.          Expr7 ::= Expr7 "(" ")";
ExFunPar.       Expr7 ::= Expr7 "(" [Expr1] ")";
ExMap.          Expr7 ::= Expr7 "|" Expr ">>";
ExPostInc.      Expr8 ::= Expr8 "++";
ExPostDec.      Expr8 ::= Expr8 "--";
ExVar.          Expr9 ::= Ident;
ExConst.        Expr9 ::= Const;
coercions Expr 9;

ExInt.          Const ::= Integer;
ExTrue.         Const ::= "true";
ExFalse.        Const ::= "false";

(:[]).          [Expr1] ::= Expr1;
(:).            [Expr1] ::= Expr1 "," [Expr1];

-- Assignments + assignments with side effects
Assign.         AssignOp ::= "=";
AssignMul.      AssignOp ::= "*=";
AssignDiv.      AssignOp ::= "/=";
AssignAdd.      AssignOp ::= "+=";
AssignSub.      AssignOp ::= "-=";

