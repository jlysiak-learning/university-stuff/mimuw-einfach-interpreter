# The EinfaCh language interpreter

Assignment project @ MIMUW 2017

## How to start?
1. Compilation: just do `make` 
2. Run: `./interpreter` or `./interpreter <my file>`

## Tests
Run: `./tests.sh full` or `make run-tests`

## TODO

Nothing...
