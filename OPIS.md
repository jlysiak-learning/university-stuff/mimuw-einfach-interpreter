
## Testy
Wszystkie testy znajdują się w katalogu `examples`.
W podkatalogu `good` umieszczone zostały poprawne konstrukcje 
języka (pliki `*.ein`) oraz komunikaty (pliki `*.out`), 
które powinny zostać wypisane w przypadku poprawnego wykonania programu.   
W podkatalogu `bad` znajdują się przykłady, które przechodzą
testy składni (`make run-syntax-tests`), ale powodują błąd
w fazie kontroli typów albo w trakcie wykonania programu.
Stosowne komunikaty błędów zamieszczone zostały w plikach `.out`.

## Zakres projektu
### Numery testów dla poszczególnych wymagań
``` 
Na 14 punktów:
+   1. (dwa typy)
        - good/00*
+   2. (arytmetyka, porównania)
        - good/01a*
        - good/01b*
+   3. (while, if)
        - good/07*
        - good/22*
+   4. (procedury lub funkcje, rekurencja)
        - good/02*
        - good/03*
        - good/04*
+   5. (print)
        - good/*
    6. 
        a) (przez zmienną i wartość) <-- tylko przez wartość
            - good/15*
+       b) (pętla for)
            - good/07*
        c) (string i rzutowania)
+       d) (wyrażenia z = ++ += itd)
            - good/09*

Na 17 punktów:
+   7. (statyczne typowanie)
            - bad/type-*

1Na 20 punktów:
+   8. (przesłanianie i statyczne wiązanie)
            - good/23*
            - good/10*
+   9. (obsługa błędów wykonania)
            - bad/runtime-*
+   10. (funkcje zwracające wartość)
            - good/04*
            - good/03*
    11. 
+       a) (rekordy)
            - good/11*
            - good/12*
            - good/14*
            - good/16*
            - good/20*
            - good/21*
            - good/24*
+       b) (tablice/listy)
            - good/06* 
            - good/12* 
            - good/13* 
            - good/14* 
            - good/17* 
            - good/18*
            - good/24* 
+       c) (słowniki)
            - good/05*
            - good/17* 
            - good/18*
            - good/19*
            - good/20*
            - good/21*
        d) (krotki z przypisaniem)
        e) (break, continue)
        f) (funkcje jako parametry)
        g) (funkcje w wyniku, domknięcia)
        h) (funkcje anonimowe)
Na 24 punkty:
+   12. (funkcje zagnieżdżone ze statycznym wiązaniem)
            - good/04*
            - good/23*
    13. (jeszcze coś lub dwa)
+       c) słowniki z punktu 11.

