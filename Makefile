# Main makefile
# Use it to build project.
#

MAIN_EXE = interpreter
SYNTAX_EXE = syntax

SRC_DIR = src
INT_OUT = build
DOC_OUT = docs
GRAMMAR = einfaCh.cf

all: $(MAIN_EXE) $(SYNTAX_EXE)

run-syntax-tests:
	./test.sh syntax examples/good/* examples/bad/*

run-tests:
	./test.sh full examples/good/* examples/bad/*

$(MAIN_EXE): build
	cp $(INT_OUT)/Main $@


$(SYNTAX_EXE): build
	cp $(INT_OUT)/TestEinfaCh $@


build: parser
	cp $(SRC_DIR)/* $(INT_OUT)
	cd $(INT_OUT) && $(MAKE)
	cd ..

LOCAL_BNFC="bnfc"
STUDENTS_BNFC="/home/students/mismap/j/jl345639/.cabal/bin/bnfc"
#BNFC=$(STUDENTS_BNFC)
BNFC=$(LOCAL_BNFC)

parser: $(GRAMMAR)
	rm -rf $(INT_OUT)
	mkdir -p $(INT_OUT)
	$(BNFC) --haskell -o $(INT_OUT) -m $(GRAMMAR)


doc: $(GRAMMAR)
	rm -rf $(DOC_OUT)
	mkdir -p $(DOC_OUT)
	bnfc --latex -o $(DOC_OUT) -m $(GRAMMAR)


.PHONY: $(DOC_OUT) $(INT_OUT) clean

clean:
	rm -rf $(INT_OUT) $(DOC_OUT) $(MAIN_EXE) $(SYNTAX_EXE)

