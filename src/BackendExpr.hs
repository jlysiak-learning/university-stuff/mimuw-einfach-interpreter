--------------------------------------------
-- Expression handling module
--
module BackendExpr(transExpr, copy) where

import System.IO (stdin, stderr, hGetContents, hPutStrLn)
import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map

import AbsEinfaCh
import ErrM

import Types
import EnvOps 
import Errors

transExpr :: Expr -> InterpreterState Value

transExpr (ExConst const) = case const of
        (ExInt i) -> return $ Zahlen (fromInteger i)
        ExTrue    -> return $ Boolean True
        ExFalse   -> return $ Boolean False

transExpr (ExVar ident)         = getValueByName ident

transExpr (ExComma e1 e2)       = transExpr e1 >> transExpr e2

transExpr (ExAssign e1 op e2)   = do
        nv      <- case op of
                Assign          -> transExpr e2
                AssignMul       -> transExpr (ExMul e1 e2)
                AssignDiv       -> transExpr (ExDiv e1 e2)
                AssignAdd       -> transExpr (ExAdd e1 e2)
                AssignSub       -> transExpr (ExSub e1 e2)
        assign e1 nv
        return nv

transExpr (ExPostInc e) = do
        ev      <- ask
        v       <- local (const ev) $ transExpr e
        _       <- transExpr $ ExAssign e AssignAdd (ExConst $ ExInt 1)
        return v

transExpr (ExPostDec e) = do
        ev      <- ask
        v       <- local (const ev) $ transExpr e
        _       <- transExpr $ ExAssign e AssignSub (ExConst $ ExInt 1)
        return v

transExpr (ExDiv e1 e2) = do
        (Zahlen v1) <- transExpr e1
        (Zahlen v2) <- transExpr e2
        if v2 == 0 then 
                throwError $ errorDivByZero (ExDiv e1 e2)
        else
                return $ Zahlen $ v1 `div` v2
transExpr (ExMul e1 e2) = exprNumBinOp (*) e1 e2
transExpr (ExSub e1 e2) = exprNumBinOp (-) e1 e2
transExpr (ExAdd e1 e2) = exprNumBinOp (+) e1 e2

transExpr (ExNeg e) = do
        (Zahlen v)       <- transExpr e
        return $ Zahlen $ v * (-1)

transExpr (ExEq e1 e2) = exprLogBinOp (==) e1 e2
transExpr (ExNEq e1 e2) = exprLogBinOp (/=) e1 e2
transExpr (ExLTh e1 e2) = exprLogBinOp (<) e1 e2
transExpr (ExGTh e1 e2) = exprLogBinOp (>) e1 e2
transExpr (ExLETh e1 e2) = exprLogBinOp (<=) e1 e2
transExpr (ExGETh e1 e2) = exprLogBinOp (>=) e1 e2

transExpr (ExArr expr idxExpr) = do
        (Zahlen i) <- transExpr idxExpr
        (Array n arr) <- transExpr expr
        if i < 0 || n <= (toInteger i) then
                throwError $ errorIndex n i
        else
                if member i arr then
                        getValueByLocation $ arr ! i
                else
                        throwError $ errorNotInitialized ()

transExpr (ExMap expr elExpr) = do
        k       <- transExpr elExpr
        (Dict m)<- transExpr expr
        if member k m then
                getValueByLocation $ m ! k
        else
                throwError $ errorElNotInDict (ExMap expr elExpr)

transExpr (ExMemb expr ident) = do
        (Struct m) <- transExpr expr
        if member ident m then
                getValueByLocation $ m ! ident
        else
                throwError $ errorNotInStruct (ExMemb expr ident)

transExpr (ExFunPar (ExVar ident) es) = do
        as      <- mapM transExpr es
        (Function f) <- getFunction ident
        f as

transExpr (ExFun (ExVar ident)) = transExpr (ExFunPar (ExVar ident) [])

------------------------------------------
-- Binary operators support
--
exprNumBinOp :: (Int -> Int -> Int) -> Expr -> Expr -> InterpreterState Value
exprNumBinOp op e1 e2 = do
        (Zahlen v1) <- transExpr e1
        (Zahlen v2) <- transExpr e2
        return $ Zahlen $ op v1 v2

exprLogBinOp :: (Int -> Int -> Bool) -> Expr -> Expr -> InterpreterState Value
exprLogBinOp op e1 e2 = do
        (Zahlen v1) <- transExpr e1
        (Zahlen v2) <- transExpr e2
        return $ Boolean $ op v1 v2

------------------------------------------
-- ASSIGNMENTS HANDLING
--

--- Definitions

-- Assign value to given expression
-- Assignments copies all structures into new location!
assign :: Expr -> Value -> InterpreterState ()

-- Localize variable in environment
localize :: Expr -> InterpreterState Location

-- Copy value
copy :: Value -> InterpreterState Value

-- Copy variable location
copyLocation :: Location -> InterpreterState Location

--- Implementation

assign expr v = do
        l       <- localize expr
        nv      <- copy v
        modify (insert l nv)

localize (ExVar ident) = getLocation ident

localize (ExArr ident idx) = do
        (Zahlen i)      <- transExpr idx
        l               <- localize ident
        v               <- getValueByLocation l
        let (n, arr) = case v of
                Void            -> (0, empty) -- Void was put into env during allocation
                (Array sz m)    -> (sz, m)
        if i < 0 || n <= (toInteger i) then
                throwError $ errorIndex n i
        else do
                if member i arr then
                        return $ arr ! i
                else do
                        l' <- alloc
                        let arr' = Array n $ insert i l' arr
                        modify (\store -> insert l arr' store)
                        return l'

localize (ExMap ident el) = do
        k       <- transExpr el
        l       <- localize ident
        v       <- getValueByLocation l
        let m = case v of
                Void    -> empty
                (Dict m')-> m'
        if member k m then do
                return $ m ! k
        else do
                l'      <- alloc
                let m' = Dict $ insert k l' m
                modify (\store -> insert l m' store)
                return l'

localize (ExMemb ident field) = do
        l       <- localize ident
        v       <- getValueByLocation l
        let s = case v of
                Void            -> empty
                (Struct s')     -> s'
        if member field s then
                return $ s ! field
        else do
                l'      <- alloc
                let s' = Struct $ insert field l' s
                modify (insert l s')
                return l'
        

copy (Zahlen i)         = return $ Zahlen i
copy (Boolean b)        = return $ Boolean b
copy (Dict dict)        = do
        newDict <- mapM copyLocation dict
        return $ Dict newDict

copy (Array sz arr)     = do 
        newArr  <- mapM copyLocation arr
        return $ Array sz newArr
copy (Struct struct)    = do
        newSt   <- mapM copyLocation struct
        return $ Struct newSt

copyLocation l = do
        l'      <- alloc
        store   <- get
        v       <- copy $ store ! l
        modify (insert l' v)
        return l'

