-- Jacek Łysiak
--
-- Interpreter internal types

module Types where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map

import AbsEinfaCh
import ErrM

type Verbosity = Int

--type Result = Err String
type Result = ExceptT String IO

-- Representation of declared types
data Value 
        = Void 
        | Zahlen Int 
        | Boolean Bool 
        | Array Integer (Map Int Location)
        | Dict (Map Value Location)
        | Struct (Map Ident Location)
        deriving (Show, Eq, Ord)

type Location = Int
type FuncName = Ident
type Variable = Ident
type Args = [Value]

type StructName = Ident
type StructType = Map Variable TypeSpec

newtype Function = Function (Args -> InterpreterState Value)

-- Main interpreter store
type SymbolsTable = Map Location Value
-- Interpreter environment
type Environment = (Map Variable Location, Map FuncName Function, Map StructName StructType)

type InterpreterState a = StateT SymbolsTable (ReaderT Environment Result) a
