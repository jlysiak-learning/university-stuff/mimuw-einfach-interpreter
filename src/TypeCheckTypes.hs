module TypeCheckTypes where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map

import AbsEinfaCh
import ErrM
import Types(Result)
import Errors

type Variable = Ident
type FunctionName = Ident
type StructName = Ident
type NestLevel = Int

data VariableType
        = Void
        | Zahlen
        | Boolean
        | Array VariableType
        | Dict VariableType VariableType
        | Struct StructName
        deriving (Eq, Show)
type FunctionType = (VariableType, [VariableType])
type StructType = Map Variable VariableType

type VariableMap = Map Variable (VariableType, NestLevel)
type FunctionMap = Map FunctionName FunctionType
type StructMap = Map StructName StructType

type TypecheckerState = (VariableMap, StructMap, FunctionMap, NestLevel)
type Typechecker a = ReaderT TypecheckerState Result a


printVariable :: Variable -> String
printVariable (Ident s) = s

getVariableMap :: Typechecker VariableMap
getVariableMap = do
        (m, _, _, _) <- ask 
        return m

setVariableMap :: Variable -> VariableType -> Typechecker TypecheckerState
setVariableMap v t = do
        (vm, sm, fm, l) <- ask
        if member v vm then do
                let (t, l') = vm ! v
                if l == l' then
                        throwError $ errorVarDeclared $ printVariable v 
                else
                        -- Variable v was declared in other scope.
                        -- Add with actual nest level
                        return (insert v (t, l) vm, sm, fm, l)
        else
                -- No such variable in scope.
                -- Add with actual nest level
                return (insert v (t, l) vm, sm, fm, l)

-- Increase checker state nest level
-- to indicate how deep we will be declaring new variables
upNestLevel :: Typechecker TypecheckerState
upNestLevel = do
        (vm, sm, fm, l) <- ask
        return (vm, sm, fm, l + 1)

setFunctionMap :: FunctionName -> FunctionType -> Typechecker TypecheckerState
setFunctionMap v t = do
        (vm, sm, fm, l) <- ask
        return (vm, sm, insert v t fm, l)

getStructMap :: Typechecker StructMap
getStructMap = do
        (_, m, _, _) <- ask
        return m

getFunctionMap :: Typechecker FunctionMap
getFunctionMap = do
        (_, _, m, _) <- ask
        return m

specToVarType :: TypeSpec -> VariableType
specToVarType TVoid     = Void
specToVarType TInt      = Zahlen
specToVarType TBool     = Boolean
specToVarType (TArr t _)= Array $ specToVarType t
specToVarType (TStruct n) = Struct n
specToVarType (TMap k v) = Dict (specToVarType k) (specToVarType v)

