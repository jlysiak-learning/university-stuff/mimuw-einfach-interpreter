module TypeCheckVariableType(
        getVariableType,
        getExprStmtType,
        getFunctionType,
        getStructType) where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map

import AbsEinfaCh
import ErrM

import Errors
import TypeCheckTypes

getFunctionType :: Expr -> Typechecker FunctionType
getFunctionType (ExVar f) = do
        funMap <- getFunctionMap
        if member f funMap then
                return $ funMap ! f
        else
                throwError $ printVariable f

getFunctionType oth = throwError $ errorNotFunc oth

getStructType :: Expr -> Typechecker StructType
getStructType expr = do
        s  <- getVariableType expr
        sm <- getStructMap
        case s of
                (Struct struct) -> do
                        if member struct sm then
                                return $ sm ! struct
                        else
                                throwError $ errorNoStruct (printVariable struct)
                _               -> do
                        throwError $ errorNotStruct expr

getVariableType :: Expr -> Typechecker VariableType

-- This rule is evaluated in e.g. sinle line multi assign
-- `a=1, b=2, c=3`
getVariableType (ExComma e1 e2) = do
        _ <- getVariableType e2
        getVariableType e1

getVariableType (ExConst const) = case const of
        (ExInt _)       -> return Zahlen
        _               -> return Boolean

getVariableType (ExVar v) = do
        vm <- getVariableMap
        if member v vm then
                return $ fst $ vm ! v
        else
                throwError $ errorNoVar (printVariable v)

getVariableType (ExAssign e1 op e2) = do
        let lv = lValue e1
        t1      <- getVariableType e1
        t2      <- getVariableType e2
        if lv then
                if t1 == t2 then
                        return t1
                else
                        throwError $ errorDiffExprTypes (ExAssign e1 op e2)
        else
                throwError $ errorNotLVal (ExAssign e1 op e2)
        where 
                lValue (ExVar _) = True
                lValue (ExMap _ _) = True
                lValue (ExMemb _ _)= True
                lValue (ExArr _ _) = True
                lValue _         = False
                
getVariableType (ExNeg e) = do
        et      <- getVariableType e
        if et == Zahlen then
                return Zahlen
        else
                throwError $ errorNotZahlenNeg (ExNeg e)

getVariableType (ExFunPar fn es) = do
        as      <- mapM getVariableType es
        (ft, pt)<- getFunctionType fn
        if pt == as then
                return ft
        else if length pt /= length as then
                throwError $ errorFunArgN (ExFunPar fn es)
        else
                throwError $ errorFunArgT (ExFunPar fn es)

getVariableType (ExFun fn) = getVariableType (ExFunPar fn [])

getVariableType (ExArr arr idx) = do
        at      <- getVariableType arr
        it      <- getVariableType idx
        case at of
                (Array t) -> do
                        if it /= Zahlen then
                                throwError $ errorNotZahlenIdx (ExArr arr idx)
                        else
                                return t
                _ -> do
                        throwError $ errorNotArray (ExArr arr idx)

getVariableType (ExMap id k) = do
        t       <- getVariableType id
        kt      <- getVariableType k
        case t of
                (Dict kt' vt) -> do
                        if kt == kt' then
                                return vt
                        else
                                throwError $ errorWrongKey (ExMap id k)
                _ ->
                        throwError $ errorNotDict (ExMap id k)

getVariableType (ExMemb id field) = do
        s       <- getStructType id
        if member field s then
                return $ s ! field
        else
                throwError $ errorNoField (ExMemb id field)

getVariableType (ExAdd e1 e2)   = checkNumBinOp e1 e2
getVariableType (ExSub e1 e2)   = checkNumBinOp e1 e2
getVariableType (ExMul e1 e2)   = checkNumBinOp e1 e2
getVariableType (ExDiv e1 e2)   = checkNumBinOp e1 e2
getVariableType (ExEq e1 e2)    = checkLogBinOp e1 e2
getVariableType (ExNEq e1 e2)   = checkLogBinOp e1 e2
getVariableType (ExLTh e1 e2)   = checkLogBinOp e1 e2
getVariableType (ExGTh e1 e2)   = checkLogBinOp e1 e2
getVariableType (ExLETh e1 e2)  = checkLogBinOp e1 e2
getVariableType (ExGETh e1 e2)  = checkLogBinOp e1 e2

getVariableType (ExPostInc e)   = getVariableType $ (ExAssign e AssignAdd (ExConst (ExInt 1)))
getVariableType (ExPostDec e)   = getVariableType $ (ExAssign e AssignSub (ExConst (ExInt 1)))


checkNumBinOp :: Expr -> Expr -> Typechecker VariableType
checkNumBinOp e1 e2 = do
        t1      <- getVariableType e1
        t2      <- getVariableType e2
        if t1 == Zahlen && t2 == Zahlen then
                return Zahlen
        else
                throwError $ errorNumOpNotZahl e1 e2
 
checkLogBinOp :: Expr -> Expr -> Typechecker VariableType
checkLogBinOp e1 e2 = do
        t1      <- getVariableType e1
        t2      <- getVariableType e2
        if t1 == Zahlen && t2 == Zahlen then
                return Boolean
        else
                throwError $ errorLogOpNotZahl e1 e2

getExprStmtType :: ExprStmt -> Typechecker VariableType
getExprStmtType StExprEnd = return Void
getExprStmtType (StExprSt e) = getVariableType e

