-------------------------------------------------
-- Error module
--
module Errors where

import PrintEinfaCh

---------------------------------
-- Interpreter errors
--
errorIndex n i          = "Index out for bounds error! Array size is " ++ (show n) ++ " but idx was " ++ (show i)

errorNotInitialized _   = "Element not initialized!"

errorElNotInDict tree   = "Requested element doesn't exists in dictionary: " ++ printTree tree

errorNotInStruct tree   = "Requested element doesn't exists in structure: " ++ printTree tree

errorDivByZero tree     = "No way! Divistion by zero is prohibited: " ++ printTree tree

---------------------------------
-- Typechecker errors
--

errorNoVar name         = "No variable like " ++ (show name) ++ " exists!"

errorNoMain _           = "`main` function not defined!"

errorIncorrectEntry _   = "Incorrect entry point! `main` cannot take any arguments!"

errorNumOpNotZahl e1 e2 = "Numeric binary operator applied to non integers: '" ++ printTree e1 ++ "' and '"++ printTree e2 ++ "'"

errorLogOpNotZahl e1 e2 = "Compare binary operator applied to non integers: '" ++ printTree e1 ++ "' and '" ++  printTree e2 ++ "'"

errorFunArgN tree       = "Wrong number of arguments in function invocation: " ++ printTree tree

errorFunArgT tree       = "Wrong arguments types in function invocation: " ++ printTree tree

errorNotLVal  tree      = "Expression is not a l-value: " ++ printTree tree

errorDiffExprTypes tree = "Diffrent expression types in assignment! " ++ printTree tree

errorFunRet fname       = "Function '" ++ (printTree fname) ++ "' returns incorrect type!"

errorLoopCond tree      =  "Loop condition is not Boolean! " ++ printTree tree

errorIfCond tree        =  "`If` condition is not Boolean! " ++ printTree tree

errorNoFunc name        = "Function " ++ (show name) ++ " not in scope!"

errorNotFunc tree       = "Not a function: \n" ++ printTree tree

errorNoStruct name      = "Struct " ++ (show name) ++ " not defined!"

errorNotStruct tree     = "It's not a structure:\n" ++ printTree tree

errorNotZahlenNeg tree  = "Tried to negate non integer value! " ++ printTree tree

errorNotZahlenIdx tree  = "Array index is non integer value! " ++ printTree tree

errorNotArray tree      = "Attempt to access element of non array variable: " ++ printTree tree

errorWrongKey tree      = "Wrong type of dictionary key used: " ++ printTree tree

errorNotDict tree       = "Object is not a dictionary: " ++ printTree tree

errorNoField tree       = "Requested field is not present in structure: " ++ printTree tree

errorNotVariable tree   = "No object exists for: " ++ printTree tree

errorVoidVariable tree  = "`void` variable cannot be declared: " ++ printTree tree

errorVarDeclared name   = "Variable `" ++ name ++ "` already declared in this scope!"
