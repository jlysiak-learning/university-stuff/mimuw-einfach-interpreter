-- Jacek Łysiak
-- Interpreter magical backend...
--

module Backend(runInterpreter) where

import System.IO (stdin, stderr, hGetContents, hPutStrLn)
import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map
import Data.List(genericReplicate)
import AbsEinfaCh
import ErrM

import Types
import EnvOps 

import BackendExpr

printV v s = when (v > 0) $ lift $ lift $ lift $ hPutStrLn stderr $ s


-- Print all kind of variables properly
valueToString :: Value -> InterpreterState String

valueToString Void              = return "(void)"

valueToString (Zahlen x)        = return $ show x

valueToString (Boolean x)       = if x then return "true" else return "false"

valueToString (Array i m)       = do
        l       <- mapM getValueByLocation m
        l'      <- mapM valueToString l
        let l'' = toList l'
        return $ "array[" ++ (show i) ++ "] = [" ++ (Prelude.foldl getStr "" l'') ++ "]"
        where
                getStr acc (_, v) = acc ++ v ++ ", "

valueToString (Dict m)          = do
        let (ks, ls)    = unzip $ toList m
        vs              <- mapM getValueByLocation ls
        vs'             <- mapM valueToString vs
        ks'             <- mapM valueToString ks
        let l''         = zip ks' vs'
        return $ "map {" ++ (Prelude.foldl getStr "" l'') ++ "}"
        where
                getStr acc (k, v) = acc ++ k ++ ": " ++ v ++ ", "

valueToString (Struct m)        = do
        l       <- mapM getValueByLocation m
        l'      <- mapM valueToString l
        let l'' = toList l'
        return $ "struct {" ++ (Prelude.foldl getStr "" l'') ++ "}"
        where
                getStr acc (Ident n, v) = acc ++ n ++ "=" ++ v ++ ", "

runInterpreter :: Verbosity -> Program -> (Result ())
runInterpreter vp prog = do
        when (vp > 0) $ lift $ putStrLn "Starting interpreter..."
        runReaderT (execStateT (transProgram vp prog) empty) (empty, empty, empty) 
        return ()

transProgram :: Verbosity -> Program -> InterpreterState ()
transProgram vp (Entry extdecls) = do
        printV vp "Loading env..."
        nev <- transExtDeclList extdecls
        (Function main) <- local (const nev) $ getFunction (Ident "main")
        printV vp "Running main function..."
        local (const nev) $ main []
        return ()

transExtDeclList :: [ExtDecl] -> InterpreterState Environment
transExtDeclList [] = ask
transExtDeclList (x:xs) = do
        env     <- transExtDecl x
        env'    <- local (const env) $ transExtDeclList xs
        return env'

transExtDecl :: ExtDecl -> InterpreterState Environment
transExtDecl x = case x of
        GVar decl             -> transDeclList [decl]
        GFun fundef           -> transFunDef fundef
        GStructDec structspec -> transStructSpec structspec

-- Initial values 
initValue :: TypeSpec -> InterpreterState Value
initValue TInt = return $ Zahlen 0
initValue TBool = return $ Boolean False
initValue (TStruct sname) = do
        desc    <- getStructType sname
        smap    <- initStruct empty $ toList desc
        return $ Struct smap
        where
                initStruct sm [] = return sm
                initStruct sm ((ident, t):xs) = do
                        l       <- alloc
                        v       <- initValue t
                        modify (\store -> insert l v store)
                        let sm' = insert ident l sm
                        initStruct sm' xs

initValue (TArr t sz) = do
        a       <- initArr sz 0 t empty
        return $ Array sz a
        where
                initArr :: Integer -> Int -> TypeSpec -> (Map Int Location) -> InterpreterState (Map Int Location)
                initArr sz i t arr = do
                        el      <- initValue t
                        l       <- alloc
                        modify (\store -> insert l el store)
                        let arr' = insert i l arr
                        if sz - 1 == (toInteger i) then
                                return arr'
                        else
                                initArr sz (i + 1) t arr'

initValue (TMap _ _)  = return $ Dict empty

transDeclList :: [Decl] -> InterpreterState Environment 
transDeclList []     = ask
transDeclList (x:xs) = do
        l       <- alloc
        v       <- initValue t
        modify (\s -> insert l v s)
        local (setLocation ident l) $ transDeclList xs
        where 
                (Declaration (DVar t ident)) = x

-- Put structure description in interpreter environment.
-- It will be used during initialization
transStructSpec :: StructSpec -> InterpreterState Environment
transStructSpec (StructDef sname ds) = do
        env     <- ask
        let struct = Prelude.foldl structFold empty ds
        return $ setStructType sname struct env
        where
                structFold acc (Declaration (DVar tspec v)) = insert v tspec acc

transArgs :: [GenDecl] -> [Value] -> InterpreterState Environment
transArgs [] [] = ask
transArgs ((DVar _ ident) : vs) (x:xs) = do
        l       <- alloc
        x'      <- copy x
        modify (\s -> insert l x' s)
        nev     <- local (setLocation ident l) $ transArgs vs xs
        return nev

transFunDefList :: [FunDef] -> InterpreterState Environment
transFunDefList [] = ask
transFunDefList (f:fs) = do
        env <- transFunDef f
        local (const env) $ transFunDefList fs

transFunDef :: FunDef -> InterpreterState Environment
transFunDef (Func _ fname args (FunctionBody decls fdefs stmts exps)) = do
        env <- ask
        let fun args' = do
                env' <- local (const env) $ transArgs args args'
                env'' <- local (const env') $ transDeclList decls
                let env''' = setFunction fname (Function fun) env''
                env'''' <- local (const env''') $ transFunDefList fdefs
                local (const env'''') $ transStmtList stmts
                case exps of
                        StExprEnd -> return $ Void
                        StExprSt exp -> local (const env'''') $ transExpr exp
        return $ setFunction fname (Function fun) env



transStmtList :: [Stmt] -> InterpreterState ()
transStmtList stmts = do
        mapM transStmt stmts
        return ()

transStmt :: Stmt -> InterpreterState ()
transStmt x = case x of
        StPrint printStmt       -> transPrintStmt printStmt
        StIter iterStmt         -> transIterStmt iterStmt
        StIf ifStmt             -> transIfStmt ifStmt
        StExpr exprStmt         -> transExprStmt exprStmt
        StComp compStmt         -> transCompStmt compStmt

transPrintStmt :: PrintStmt -> InterpreterState ()
transPrintStmt (PrintBase expr) = do
        val <- transExpr expr
        str <- valueToString val
        lift $ lift $ lift $ putStrLn str

transIterStmt :: IterStmt -> InterpreterState ()
transIterStmt (StIterWhile e stmt) = do
        (Boolean cond)  <- transExpr e
        if cond then
                transStmtList [stmt, (StIter (StIterWhile e stmt))]
        else
                return ()

-- Transform for into while statement
transIterStmt (StIterForThree stInit stCond eOp loopStmt) = do
        let l = newLoop loopStmt eOp
        let c = newCond stCond
        transStmtList [(StExpr stInit), (StIter (StIterWhile c l))]
        where
                newCond c = case c of
                        StExprEnd       -> ExConst ExTrue
                        (StExprSt e)    -> e
                newLoop s op = StComp (StCompSt [] [s, (StExpr (StExprSt op))])

-- for (e1; e2;) -> for (e1; e2; True)
transIterStmt (StIterForTwo stInit stCond loopStmt) =
        transIterStmt (StIterForThree stInit stCond (ExConst ExTrue) loopStmt)
        

transIfStmt :: IfStmt -> InterpreterState ()
transIfStmt (StIfOnly cond stmt) = do
        (Boolean c) <- transExpr cond
        if c then
                transCompStmt stmt
        else
                return ()
transIfStmt (StIfElse cond ifTrue ifFalse) = do
        (Boolean c) <- transExpr cond
        if c then
                transCompStmt ifTrue
        else
                transCompStmt ifFalse

transExprStmt :: ExprStmt -> InterpreterState ()
transExprStmt StExprEnd = return ()
transExprStmt (StExprSt e) = transExpr e >> return ()

transCompStmt :: CompStmt -> InterpreterState ()
transCompStmt (StCompSt dlist stmts) = do
        nev <- transDeclList dlist
        local (const nev) $ transStmtList stmts
