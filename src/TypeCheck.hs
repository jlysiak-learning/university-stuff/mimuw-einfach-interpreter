-- Jacek Łysiak
-- Static type checking module
--
module TypeCheck(staticCheck) where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map

import AbsEinfaCh
import ErrM

import Errors
import Types(Verbosity, Result)
import TypeCheckTypes
import TypeCheckVariableType

mapp = Prelude.map
foldlp = Prelude.foldl

staticCheck :: Verbosity -> Program -> Result ()
staticCheck vp program = do
        when (vp > 0) $ lift $ putStrLn "Running TypeChecker..."
        runReaderT (programChecker program) (empty, empty, empty, 0)
        return ()
 
programChecker :: Program -> Typechecker ()
programChecker (Entry exts) = do
        e       <- extDeclsChecker exts
        fm      <- local (const e) $ getFunctionMap
        if member (Ident "main") fm then do
                (_, args)       <- local (const e) $ getFunctionType (ExVar (Ident "main"))
                if args /= [] then
                        throwError $ errorIncorrectEntry () 
                else
                        return ()
        else
                throwError $ errorNoMain ()

extDeclsChecker :: [ExtDecl] -> Typechecker TypecheckerState
extDeclsChecker []      = ask
extDeclsChecker (x:xs)  = do
        ne      <- extDeclChecker x
        local (\_ -> ne) $ extDeclsChecker xs

extDeclChecker :: ExtDecl -> Typechecker TypecheckerState
extDeclChecker x = case x of
        GVar decl             -> declListChecker [decl]
        GFun fundef           -> funDefChecker fundef
        GStructDec structspec -> structSpecChecker structspec

declListChecker :: [Decl] -> Typechecker TypecheckerState
declListChecker [] = ask
declListChecker (x:xs) = do
        let t' = specToVarType t
        if t' == Void then
                throwError $ errorVoidVariable x
        else do
                vm      <- getVariableMap
                e       <- setVariableMap ident t'
                local (const e) $ declListChecker xs
                where 
                        (Declaration (DVar t ident)) = x

funDefListChecker :: [FunDef] -> Typechecker TypecheckerState
funDefListChecker [] = ask
funDefListChecker (f:fs) = do
        ne      <- funDefChecker f
        local (const ne) $ funDefListChecker fs

funDefChecker :: FunDef -> Typechecker TypecheckerState
funDefChecker (Func typespec fname args fbody ) = do
        e       <- declListChecker $ mapp (\x -> (Declaration x)) args
        e       <- local (const e) $ upNestLevel
        e'      <- local (const e) $ declListChecker decls
        e''     <- local (const e') $ setFunctionMap fname ft
        e'''    <- local (const e'') $ funDefListChecker fdefs
        local (const e''') $ stmtListChecker stmts
        rt'     <- local (const e''') $ getExprStmtType exps
        if rt == rt' then
                setFunctionMap fname ft
        else
                throwError $ errorFunRet fname
        where
                (FunctionBody decls fdefs stmts exps) = fbody
                pt = mapp (\(DVar t _) -> specToVarType t) args
                rt = specToVarType typespec 
                ft = (rt, pt)

structSpecChecker :: StructSpec -> Typechecker TypecheckerState
structSpecChecker (StructDef id ds) = do
        (vm, sm, fm, l)    <- ask
        let struct = foldlp structFold empty ds
        return (vm, insert id struct sm, fm, l)
        where
                structFold acc (Declaration (DVar tspec v)) = insert v (specToVarType tspec) acc


stmtListChecker :: [Stmt] -> Typechecker ()
stmtListChecker ss = do
        mapM stmtChecker ss
        return ()

stmtChecker :: Stmt -> Typechecker ()
stmtChecker s = case s of
        StPrint printStmt       -> printStmtChecker printStmt
        StIter iterStmt         -> iterStmtChecker iterStmt
        StIf ifStmt             -> ifStmtChecker ifStmt
        StExpr exprStmt         -> exprStmtChecker exprStmt
        StComp compStmt         -> compStmtChecker compStmt

        
printStmtChecker :: PrintStmt -> Typechecker ()
printStmtChecker (PrintBase expr) = do
        _       <- getVariableType expr
        return ()
        
ifStmtChecker :: IfStmt -> Typechecker ()
ifStmtChecker (StIfOnly cond stmt) = ifStmtChecker (StIfElse cond stmt (StCompSt [] []))
ifStmtChecker (StIfElse cond ifTrue ifFalse) = do
        c       <- getVariableType cond
        if c /= Boolean then
                throwError $ errorIfCond cond
        else do
                _       <- compStmtChecker ifTrue
                _       <- compStmtChecker ifFalse
                return ()

compStmtChecker :: CompStmt -> Typechecker ()
compStmtChecker (StCompSt dlist stmts) = do
        nev <- declListChecker dlist
        local (const nev) $ stmtListChecker stmts

exprStmtChecker :: ExprStmt -> Typechecker ()
exprStmtChecker StExprEnd = return ()
exprStmtChecker (StExprSt e) = getVariableType e >> return ()
        
iterStmtChecker :: IterStmt -> Typechecker ()
iterStmtChecker (StIterForThree stInit stCond eOp loopStmt) = do
        _       <- getExprStmtType stInit
        c       <- getExprStmtType stCond
        _       <- getVariableType eOp
        if c == Boolean then
                stmtChecker loopStmt
        else
                throwError $ errorLoopCond stCond

-- Just transform to full `for` expression
iterStmtChecker (StIterForTwo stInit stCond loopStmt) =
        iterStmtChecker (StIterForThree stInit stCond (ExConst ExTrue) loopStmt)
-- Same here
iterStmtChecker (StIterWhile e stmt) =
        iterStmtChecker (StIterForTwo (StExprSt (ExConst ExTrue)) (StExprSt e) stmt)

