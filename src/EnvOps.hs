-- Jacek Łysiak
--
-- Interpreter state operations
--

module EnvOps(
        getLocation,
        setLocation,
        getFunction,
        setFunction,
        getValueByName,
        getValueByLocation,
        setValue,
        setStructType,
        getStructType,
        alloc) where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.State
import Data.Map
import Types

-- Environment state operations
getLocation :: Variable -> InterpreterState Location
getLocation v = do
        (vm, _, _) <- ask
        return $ vm ! v

setLocation :: Variable -> Location -> Environment -> Environment
setLocation v l (varEnv, funEnv, strDesc) = (varEnv', funEnv, strDesc) where varEnv' = insert v l varEnv

getFunction :: FuncName -> InterpreterState Function 
getFunction fn = do
        (_, fm, _) <- ask
        return $ fm ! fn

setFunction :: FuncName -> Function -> Environment -> Environment
setFunction fn f (varEnv, funEnv, strDesc) = (varEnv, funEnv', strDesc) where funEnv' = insert fn f funEnv

getStructType :: StructName -> InterpreterState StructType
getStructType sn = do
        (_, _, sm) <- ask
        return $ sm ! sn

setStructType :: StructName -> StructType -> Environment -> Environment
setStructType sn st (varEnv, funEnv, strDesc) = (varEnv, funEnv, strDesc') where strDesc' = insert sn st strDesc

-- Symbols Table interface
getValueByName :: Variable -> InterpreterState Value
getValueByName v = do
        sym <- get
        l   <- getLocation v
        return $ sym ! l

getValueByLocation :: Location -> InterpreterState Value
getValueByLocation l = do
        sym <- get
        return $ sym ! l

setValue :: Variable -> Value -> InterpreterState ()
setValue v val = do
        l <- getLocation v
        modify $ insert l val
        
-- Allocate space in symbols table
-- Put `Void` at the begining and this will be overwriten later
alloc :: InterpreterState Location
alloc = do
        sym <- get
        let l = if size sym /= 0 then (fst $ findMax sym) + 1 else 1
        put $ insert l Void sym
        return l
