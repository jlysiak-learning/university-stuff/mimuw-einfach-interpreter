-- Jacek Łysiak
-- Interpreter entry point
--
module Main where

import System.IO ( stdin, stderr, hGetContents, hPutStrLn )
import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )
import System.Console.GetOpt
import Control.Monad.Except

import LexEinfaCh
import ParEinfaCh
import AbsEinfaCh
import ErrM

import Backend
import TypeCheck

-- TO DO later ... 
data Opts = Opts { optVerbose :: Int }

defaultOpts :: Opts
defaultOpts = Opts { optVerbose = 0 }
opts :: [OptDescr (Opts -> IO Opts)]
opts = [
        Option "v" ["verbose"] (NoArg (\opt -> return opt { optVerbose = 1 })) "Enable verbose messages"
       ]
        

run vp input =  case pProgram tokens of
        Bad msg -> do
                hPutStrLn stderr $ "**** Parse error!\n" ++ msg
                exitFailure
        Ok prog -> do
                checkResult <- runExceptT $ staticCheck vp prog
                case checkResult of
                        (Left e) -> do
                                hPutStrLn stderr $ "**** Static type check error!\n" ++ e
                                exitFailure
                        _ -> do
                                runResult <- runExceptT $ runInterpreter vp prog
                                case runResult of
                                        (Left e) -> do
                                                hPutStrLn stderr $ "**** Runtime error!\n" ++ e
                                                exitFailure
                                        _ -> exitSuccess
        where tokens = myLexer input

main :: IO ()
main = do
        args <- getArgs
        let (vp, file) = if args /= [] && (head args) == "-v" then (1, tail args) else (0, args)
        content <- case file of
                []      -> hGetContents stdin
                (fn:_)  -> readFile fn
        run vp content



