#!/bin/bash

TMP=tmp.out


echo "**** EinfaCh language interpreter tests"

function run_syntax_tests {
        loc=${1%/}
        for el in $(ls -1 ${loc}/*.ein);
        do
                cat $el | ./syntax > $TMP
                res=$(cat $TMP| grep "Success")
                if [ -z "$res" ]; then
                        echo -e "[\033[91mFAIL\033[0m]: \033[93m$el\033[0m"
                        cat $TMP
                else
                        echo -e "[ \033[92mOK\033[0m ]: \033[93m$el\033[0m"
                fi
                rm $TMP
        done
}

function run_tests {
        loc=${1%/}
        for el in $(ls -1 ${loc}/*.ein);
        do
                fullname=$(basename ${el})
                basename=$(echo $fullname | cut -d"." -f 1)
                cmp=${loc}/${basename}.out

                cat $el | ./interpreter &> $TMP
                diff $TMP $cmp &> /dev/null
                if [ $? -gt 0 ]; then
                        echo -e "[\033[91mFAIL\033[0m]: \033[93m$el\033[0m"
                        diff $TMP $cmp
                else
                        echo -e "[ \033[92mOK\033[0m ]: \033[93m$el\033[0m"
                fi
                rm $TMP
        done
}

case $1 in
        full)
                echo "Running full tests..."
                echo
                echo "GOOD examples:"
                run_tests examples/good/
                echo
                echo "BAD examples:"
                run_tests examples/bad/
                echo
                echo "Done!"
                ;;

        syntax)
                echo "Running syntax tests..."
                echo
                echo "GOOD examples:"
                run_syntax_tests examples/good/
                echo
                echo "BAD examples:"
                run_syntax_tests examples/bad/
                echo
                echo "Done!"
                ;;
        *)
                echo "Usage: ./test.sh {full|syntax}"
                echo "Bye!"
                ;;
esac

