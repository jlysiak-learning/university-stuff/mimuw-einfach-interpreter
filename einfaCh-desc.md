# einfa**C**h 
Autor: Jacek Łysiak

## Opis 
Język einfa**C**h jest pochodną języka C - bardzo uproszczoną.
Składnia i konstrukcje zostały zeń zapożyczone.

W trakcie prac nad gramatyką języka posiłkowałem się, 
dostępną w repozytorium BNFC, gramatyką języka C.  
Odnośnik do materiału: [LINK](https://github.com/BNFC/bnfc/blob/master/examples/C/C.cf)

Planowane cechy:
1. Typy: `int` i `bool`
2. Arytmetyka i porównania
3. `while (cond)`, `if (cond)`, `if (cond) {...} else {...}`
4. Funkcje i procedury 
    - przekazywanie prametrów przez wartość
    - rekurencja
    - zagnieżdżanie
    - zwracanie wartości przez funkcję
5. Jawne wypisywanie wartości zmiennej na wyjście - instrukcja `print( Expr )`
6. `for (... ; ... ; ...)` bez możliwości deklaracji zmiennej
7. wyrażenia z efektami ubocznymi: `+=`, `-=`, `*=`, `/=`, `x++`, `x--`
8. Statyczne typowanie
9. Przesłanianie identyfikatorów ze statycznym ich wiązaniem
10. Jawnie obsłużone dynamiczne błędy wykonania, np. dzielenie przez zero, wyjście poza tablicę
11. Rekordy (struktury z C)
12. Tablice indeksowane `int`
13. Słowniki `map<key_type, value_type> var_name`, `key_type` musi być porównywalny

Założenia:  
- poprawny program musi zawierać definicję funkcji `void main()`, która jest punktem wejściowym obliczeń
- pętla `for` wymaga wcześniejszego zadeklarowania zmiennych, nie możemy wykonać kodu `for (int i = 0; ...`
- struktury należy definiować na poziomie globalnym
- typ `void` zarezerwowany jest dla procedur, nie będzie można zadeklarować takiej zmiennej
- dla uproszczenia, każda funkcja musi zakończć się instrukcją `return Expr;`, procedura kończy się pustym `return;`
- uproszczenie definicji funkcji, należy zachować kolejność:
    1. deklaracje zmiennych
    2. definicje funkcji
    3. instrukcje

## Gramatyka
Gramatyka języka einfa**C**h znajduje się w osobnym pliku `einfaCh.cf`.

## Zakres projektu - tabelka
``` 
Na 14 punktów:
+   1. (dwa typy)
+   2. (arytmetyka, porównania)
+   3. (while, if)
+   4. (procedury lub funkcje, rekurencja)
+   5. (print)
    6. 
        a) (przez zmienną i wartość)
+       b) (pętla for)
        c) (string i rzutowania)
+       d) (wyrażenia z = ++ += itd)

Na 17 punktów:
+   7. (statyczne typowanie)

Na 20 punktów:
+   8. (przesłanianie i statyczne wiązanie)
+   9. (obsługa błędów wykonania)
+   10. (funkcje zwracające wartość)
    11. 
+       a) (rekordy)
+       b) (tablice/listy)
+       c) (słowniki)
        d) (krotki z przypisaniem)
        e) (break, continue)
        f) (funkcje jako parametry)
        g) (funkcje w wyniku, domknięcia)
        h) (funkcje anonimowe)
Na 24 punkty:
+   12. (funkcje zagnieżdżone ze statycznym wiązaniem)
    13. (jeszcze coś lub dwa)
+       c) słowniki z punktu 11.

Ocena: 24 (zgodnie ze standardową punktacją)
```

## Przykłady

#### Podstawowy program
```c
int a;
bool b;

define void main() {
        a = 1;
        b = true;
        print(a);       // Wypisz: 1
        print(b);       // Wypisz: true
        return;
}
```

#### Proste wyrażenia
```c
int a;
int b;
int c;

bool r;

define void main() {
        a = 42;
        b = 24;
        c = 7;
        print(a + b);
        print(a - b);
        print(a * b);
        print(a / c);
        print(a + b * c);
        print(a * b + c);
        print(a * (b + c));

        r = a < b;
        print(r);
        print(a++);
        print(a);
        print(b--);
        print(b);

        print(a >= b);
        print(a >= a);
        print(b >= a);

        print(c *= 3);
        print(c /= 3);
        print(c += 32);
        print(c -= 22);
        return;
}
```

#### Użycie funkcji
```c
define int add(int a, int b) {
        return a + b;
}

define void main() {
        print(add(40, 2));
        return;
}
```

#### Wykorzystanie słownika
```c
map<int, int> ii;
map<bool, int> bi;
map<int, bool> ib;
map<bool, bool> bb;

define void main () {
        ii|1>> = 1;
        ii|2>> = 42;
        bi|false>> = 2;
        bi|true>> = 2;
        ib|2>> = true;
        ib|5>> = false;
        bb|true>> = false;
        bb|false>> = true;
        
        print(ii|1>>);
        print(ii|2>>);
        print(bi|false>>);
        print(bi|true>>);
        print(ib|2>>);
        print(ib|5>>);
        print(bb|true>>);
        print(bb|false>>);
        
        return;
}
```

#### Rekurencja
```c
define int fac(int k) {
        int r;
        if (k <= 1) {
                r = 1;
        } else {
                r = fac(k - 1) * k;
        }
        return r;
}

define void main() {
        print(fac(5));
        return;
}
```

#### Zagnieżdżanie funkcji
```c 
define void foo(int a, int b) {
        int i;
        define int goo(int a, int b) {
                return a + b;
        }
        for (i = a; i < b; i++) {
                print(goo(1, i));
        }
        return;
}

define void main() {
        foo(0, 10);
        return;
}
```

#### Wykorzystanie struktury i tablicy
```c
int i;
int j;
struct point {
        int x;
        int y;
        bool ok;
}

struct point[100][100] points;

define void main() {
        for (i = 0; i < 100; i++) {
                for (j = 0; j < 100; j++) {
                        points[i][j].x = i;
                        points[i][j].y = j;
                        if (i * i + j * j < 5 * 5) {
                                points[i][j].ok = true;
                        } else {
                                points[i][j].ok = false;
                        }
                }
        }
        for (i = 0; i < 100; i++) {
                for (j = 0; j < 100; j++) {
                        if (points[i][j].ok) {
                                print(points[i][j].x);
                                print(points[i][j].y);
                        }
                }
        }
        return;
}
```
